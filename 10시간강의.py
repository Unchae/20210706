# # containers
# list = []
# tuple = () .update
#
# set = {} #유니크 값만 유지, 순서 중요 x #  .update, .discard ,.remove(discard와 달리 에러가 남)
# # union = A|B, A.intersection(B), A.difference(B),
# # A.symmetric_difference(B), A.issubset(B) A에 있는 값들이 B에도 있는가?
# frozenset = {} #    자료를 한 번 만들고 추가할 수가 없음..add사용불가.
# dictionary = {}
# vip_names = ['a', 'b', 'c', 'd', 'e']
# door1_list = ['a']
# door2_list = ['b','c']
# print(door1_list[0] in vip_names)
# all = set(vip_names)
# print(all)
#
# d1 = set(door1_list)
# d2 = set(door2_list)
# print(d1)
# print(d2)
#
# print(d1.issubset(all))
#
# print(d2.issubset(all))
#
# d_combined = d1|d2
# print(d_combined)
# print(d_combined.issubset(all))


